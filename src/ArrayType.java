import java.util.Scanner;

public class ArrayType {
    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        int arraySize=sc.nextInt() ;
        int[] array=new int[arraySize ];
        for(int i=0;i<arraySize ;i++){
            int arrayElement=sc.nextInt() ;
            array[i]=arrayElement;
        }
        int arrayType=findType(array) ;
        if (arrayType ==1){
            System.out.println("Even");
        }
        else if(arrayType==2){
            System.out.println("Odd");
        }
        else{
            System.out.println("Mixed");
        }

    }
    static int findType(int[] array){
        int odd=0;
        int even=0;
        for (int element:array
        ) {
            if (element%2==0){
                even+=1;
            }
            else{
                odd+=1;
            }
        }
        if (even==array.length ){
            return 1;
        }
        else if(odd==array.length){
            return 2;
        }
        else{
            return 3;
        }
    }
}
